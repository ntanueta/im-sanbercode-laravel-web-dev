@extends('master.master')

@section('title')
    Halaman Register
@endsection

@section('content')

    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        
        <label for="firstName">First name:</label> <br> <br>
        <input type="text" name="firstName">
        <br><br>
        
        <label for="lastName">Last name:</label> <br> <br>
        <input type="text" name="lastName">
        <br><br>
        
        <label for="gender">Gender</label> <br> <br>
        <input type="radio" name="gender" value="male"> Male <br>
        <input type="radio" name="gender" value="female"> Female <br>
        <input type="radio" name="gender" value="other"> Other <br> <br>
        
        <label for="nationality">Nationality:</label> <br> <br>
        <select name="nationality">
            <option value="indonesia">Indonesian</option>
            <option value="singapore">Singaporean</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select> <br> <br>
        
        <label for="language">Language Spoken:</label> <br> <br>
        <input type="checkbox" name="language" value="indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="english"> English <br>
        <input type="checkbox" name="language" value="other"> Other <br> <br>
        
        <label for="bio">Bio:</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>
        
        <input name="submit" type="submit" value="Sign Up">
    </form>

@endsection
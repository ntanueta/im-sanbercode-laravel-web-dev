<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('pages.register');
    }

    public function welcome(Request $request) {
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        // $gender = $request['gender'];
        // $nationality = $request['nationality'];
        // $language = $request['language'];
        // $bio = $request['bio'];

        return view('pages.welcome', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}

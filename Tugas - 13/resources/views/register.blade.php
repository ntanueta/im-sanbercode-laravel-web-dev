<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Page</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="post" action="/welcome">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>

        <label>Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label>language Spoken:</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>

        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
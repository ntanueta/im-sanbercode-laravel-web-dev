<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day 1 - HTML | Home </title>
</head>
<body>
    <h1>Buat Accoun Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="first_name">
        <br><br>

        <label>Last Name</label><br>
        <input type="text" name="last_name">
        <br><br>

        <label>Gender</label><br>
        <input type="radio">Male<br>
        <input type="radio">Famale<br>
        <input type="radio">Other<br>
        <br>

        <label>Nasionality:</label><br>
        <select>
            <option>Indonesian</option>
            <option>Malaysian</option>
            <option>Singaporean</option>
            <option>Australian</option>
        </select>
        <br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <br>
        
        <label>Bio:</label><br>
        <textarea cols="30" rows="10"></textarea><br>
        <input type="submit" value="Register">
    </form>
</body>
</html>